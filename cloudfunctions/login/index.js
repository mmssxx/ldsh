// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV}) // 使用当前云环境

// 云函数入口函数
const db = cloud.database()
// 注意这里的 album 正是在数据库中新增的集合
// exports.main = async (event, context) => {
// 	return db.collection("user").get()
// }
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  // 获取用户登录凭证 code
  const code = event.code;

  try {
    // 调用微信 API，换取用户 openid 和 session_key
    const res = await cloud.callFunction({
      name: 'wxapi',
      data: {
        api: 'login',
        code
      }
    });

    const { openid, session_key } = res.result;

    // 存储用户登录态到数据库中
    const db = cloud.database();
    const collection = db.collection('user');
    const result = await collection.where({ openid }).get();

    if (result.data.length === 0) {
      // 如果用户不存在，则添加新用户
      await collection.add({
        data: {
          openid,
          session_key,
          createTime: new Date(),
        },
      });
    } else {
      // 如果用户已存在，则更新 session_key
      await collection.doc(result.data[0]._id).update({
        data: {
          session_key,
          updateTime: new Date(),
        },
      });
    }

    return {
      openid,
      session_key,
    };
  } catch (error) {
    console.log(error);
    return error;
  }
}