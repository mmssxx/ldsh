// pages/home/home.js
const db = wx.cloud.database()
const pinyin = require('wl-pinyin');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeKey: 0,
    active: 0,
    categoryList: [
      {categoryName:"全部"},
      {categoryName:"会长"},
      {categoryName:"创会会长"},
      {categoryName:"执行会长"},
      {categoryName:"监事长"},
      {categoryName:"常务会长"},
      {categoryName:"秘书长"},
      {categoryName:"副会长"},
      {categoryName:"理事"},
    ],
    userInfoList:[],
    searchValue:'',
    AlphabetList : ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "W", "X", "Y", "Z"],
    memberList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    
    const a = pinyin.default.getFirstLetter('中国')
    console.log(a)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    const db = wx.cloud.database()
    db.collection("user_info").get({
      success: res => {
        console.log(res)
        let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        // console.log(this.data.userInfoList)
      }
    });
  },
  onChange(event){
    const db = wx.cloud.database();
    if(event.detail.name==1) {
      db.collection("member_enterprise").get({
        success: res => {
          console.log(res)
          this.setData({
            memberList: res.data
          });
        }
      })
    }
  },
  getUserInfo(event) {
    const detail = event.detail;
    if (detail === 0) {
      // 查询 user_info 表里的所有数据
      db.collection("user_info").get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    } else if (detail === 1) {
      // 查询表里 duties 为 "党支部" 的数据
      db.collection("user_info").where({
        duties: "会长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    } else if (detail === 2) {
      db.collection("user_info").where({
        duties: "创会会长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }else if (detail === 3) {
      db.collection("user_info").where({
        duties: "执行会长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }else if (detail === 4) {
      db.collection("user_info").where({
        duties: "监事长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }else if (detail === 5) {
      db.collection("user_info").where({
        duties: "常务会长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }else if (detail === 6) {
      db.collection("user_info").where({
        duties: "秘书长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }else if (detail === 7) {
      db.collection("user_info").where({
        duties: "副会长"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }else if (detail === 8) {
      db.collection("user_info").where({
        duties: "理事"
      }).get({
        success: res => {
          let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        }
      });
    }
  },
  onSearch(event) {
    const value = event.detail;

    // 连接数据库并查询 user_info 表中的人名
    db.collection("user_info").where({
      name: db.RegExp({
        regexp: value,
        options: 'i' // 不区分大小写
      })
    }).get({
      success: res => {
        const names = res.data.map(item => item.name);
        console.log('查询结果:', names);
        let employeeNameList = [];
        for (let k in res.data) {
          employeeNameList.push({name: res.data[k].name, duties: res.data[k].duties,company:res.data[k].company,companyDuties:res.data[k].companyDuties,headImg:res.data[k].headImg});
        }
        let sortedEmployeeList = employeeNameList.sort((a, b) => {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
        let firstName = {};
        this.data.AlphabetList.forEach((item) => {
          firstName[item] = [];
          sortedEmployeeList.forEach((employee) => {
            let fullPinyin = pinyin.default.getFirstLetter(employee.name);
            let firstLetter = fullPinyin.charAt(0, 1);
            if (firstLetter === item) {
              firstName[item].push(employee);
            }
          });
        });
  
        this.setData({
          userInfoList: firstName
        });
        // 更新页面数据或其他操作
      },
      fail: err => {
        console.error('查询失败:', err);
      }
    });
  },
  methods: {
    
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log('onUnload查询失败:', err);
  },
  
  /**
   * 昌
   */
  onUnloadRR() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})