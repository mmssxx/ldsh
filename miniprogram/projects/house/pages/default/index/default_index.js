const pageHelper = require('../../../../../helper/page_helper.js');
// const cloudHelper = require('../../../../../helper/cloud_helper.js');
// const ProjectBiz = require('../../../biz/project_biz.js');
// const PassportBiz = require('../../../../../comm/biz/passport_biz.js');

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
    dataList:[]
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	// onLoad: async function (options) {
	// 	ProjectBiz.initPage(this);
  // },
  onShow() {
    this.getNews()
  },
  getNews() {
    const db = wx.cloud.database();
    db.collection('communication')
    .orderBy('creatTime', 'desc')
    .get({
      success: (res) => {
        
        this.setData({
          dataList:res.data
        })
        console.log(this.data.dataList);
      },
      fail: (error) => {
        console.error('提交失败', error);
        // 处理提交失败后的操作
      }
    });
  },
	_loadList: async function () {
		let opts = {
			title: 'bar'
		}
		await cloudHelper.callCloudSumbit('home/list', {}, opts).then(res => {
			let newsList = [];
			for (let k = 0; k < res.data.length; k++) {
				if (res.data[k].type == 'news' && res.data[k].cateId == 1)
					newsList.push(res.data[k]);
			}

			this.setData({
				newsList,
				dataList: res.data
			});

		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () { },

	/**
	 * 生命周期函数--监听页面显示
	 */
	// onShow: async function () {
	// 	PassportBiz.loginSilence(this);
	// 	this._loadList();
	// },

	onPullDownRefresh: async function () {
		await this._loadList();
		wx.stopPullDownRefresh();
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	url: async function (e) {
		pageHelper.url(e, this);
	},


	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},
})