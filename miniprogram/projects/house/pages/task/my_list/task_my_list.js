// pages/mine/mine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    code:'',
    userName:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  login() {
    wx.login({
      success: (res) => {
        const code = res.code;
        const userName = this.generateRandomString(10); // 生成长度为10的随机字符串
        this.setData({
          code,
          userName
        });
        const db = wx.cloud.database();
        db.collection('users').add({
          data: {
            code,
            userName
          },
          success: (res) => {
            console.log('添加成功', res);
            // 处理提交成功后的操作
            const id = res._id
            wx.switchTab({
              url: '/projects/house/pages/my/index/my_index?id='+id,
            })
          },
          fail: (error) => {
            console.error('提交失败', error);
            // 处理提交失败后的操作
          }
        });
      }
    });
    // wx.cloud.callFunction({
    //   name:'login',
    //   success:(res)=> {
    //     console.log(res)
        
    //   }
    // })
    
  },
  generateRandomString(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
})