// pages/news/news.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    newsList:[],
    recruitList:[]
  },
  onChange(event) {
    const db = wx.cloud.database();
    if(event.detail.name==0) {
      db.collection("communication").where({
        selectCategory: 1
      }).get({
        success: res => {
          const formattedData = res.data.map(item => {
            const date = new Date(item.creatTime);
            const year = date.getFullYear();
            const month = date.getMonth() + 1;
            const day = date.getDate();
            const formattedTime = `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
            return {
              ...item,
              creatTime: formattedTime
            };
          });
          
          this.setData({
            newsList: formattedData
          });
          // console.log(this.data.newsList);
        }
      });
    }else if(event.detail.name==1) {
      db.collection("communication").where({
        selectCategory: 2
      }).get({
        success: res => {
          this.setData({
            newsList: res.data
          });
          console.log(this.data.newsList)
        }
      })
    }else if(event.detail.name==2) {
      db.collection("communication").where({
        selectCategory: 3
      }).get({
        success: res => {
          this.setData({
            newsList: res.data
          });
          console.log(this.data.newsList)
        }
      })
    }else if(event.detail.name==3) {
      db.collection("recruit").get({
        success: res => {
          this.setData({
            recruitList: res.data
          });
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log(this.data.selectCategory)
    const db = wx.cloud.database();
    const { selectCategory } = this.data;
    db.collection("communication").where({
      selectCategory: 1
    }).get({
      success: res => {
        const formattedData = res.data.map(item => {
          const date = new Date(item.creatTime);
          const year = date.getFullYear();
          const month = date.getMonth() + 1;
          const day = date.getDate();
          const formattedTime = `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
          return {
            ...item,
            creatTime: formattedTime
          };
        });
        
        this.setData({
          newsList: formattedData
        });
        // console.log(this.data.newsList);
      }
    });
  },
  goDetail(event) {
    const id = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/newsDetail/newsDetail?id=' + id, // 跳转到对应的详情页
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})