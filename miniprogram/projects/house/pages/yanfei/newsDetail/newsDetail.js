// pages/newsDetail/newsDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    detailList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const { id } = options; // 获取传递过来的id参数
    this.setData({ id });
    const db = wx.cloud.database();
    db.collection("communication").where({
      _id: id
    }).get({
      success: res => {
        const formattedData = [];
        // 遍历 res.data 对象的属性值，将每个属性值转换为格式化后的对象并添加到 formattedData 数组中
        for (const key in res.data) {
          if (Object.hasOwnProperty.call(res.data, key)) {
          const item = res.data[key];
          const date = new Date(item.creatTime);
          const year = date.getFullYear();
          const month = date.getMonth() + 1;
          const day = date.getDate();
          const formattedTime = `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
          formattedData.push({
            ...item,
            creatTime: formattedTime
          });
        }
      }
        this.setData({
          detailList: formattedData
        });
        console.log(this.data.detailList)
      }
    })
},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})