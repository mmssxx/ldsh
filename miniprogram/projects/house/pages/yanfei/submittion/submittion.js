// pages/submittion/submittion.js
Page({
  data: {
    formats: {},
    readOnly: false,
    placeholder: '开始输入...',
    keyboardHeight: 0,
    isIOS: false,
    dataArry:'',
    selectCategory:1,
    newsTitle:'',
    creatTime:Date.now(),
    position:'',
    positionType:'',
    workLocation:'',
    feedBack:[],
    fileList:[]
  },
  positionInput(event) {
    console.log(event.detail);
    this.setData({
      position:event.detail
    })
  },
  positionTypeInput(event) {
    console.log(event.detail);
    this.setData({
      positionType:event.detail
    })
  },
  workLocationInput(event) {
    console.log(event.detail);
    this.setData({
      workLocation:event.detail
    })
  },
  handleSelectCategory(event) {
    const category = parseInt(event.currentTarget.dataset.category);
    this.setData({
      selectCategory: category
    })
  },
  newsTitle(event) {
    console.log(event.detail);
    this.setData({
      newsTitle:event.detail
    })
  },
  onEditorReady() {
    const that = this
    wx.createSelectorQuery().select('#editor').context(function (res) {
      that.editorCtx = res.context
    }).exec()
  },

  onStatusChange(e) {
    const formats = e.detail
    this.setData({ formats })
    console.log(e.detail)
  },
  goplay(e) {
    return new Promise((resolve, reject) => {
      this.setData({
        dataArry: e.detail.html
      }, () => {
        resolve();
      });
    });
  },
  afterRead(file) {
    const { feedBack } = this.data;
        feedBack.push({url:file.detail.file.tempFilePath});
        this.setData({ feedBack });
        console.log(feedBack);
    console.log(file.detail.file.tempFilePath)
    const timestamp = Date.now();
    wx.cloud.uploadFile({
      cloudPath: `upload_${timestamp}_${file.name}`,
      filePath: file.detail.file.tempFilePath,
      success: res => {
        console.log('上传成功', res.fileID);
        const { fileList } = this.data;
        fileList.push(res.fileID);
        this.setData({ fileList });
        console.log(fileList);
      },
      fail: err => {
        console.error('上传失败', err);
      }
    });
  },
  async onSubmit(e) {
    const db = wx.cloud.database();
    await this.goplay(e);
    const dataArry = this.data.dataArry;
    const { selectCategory,creatTime,newsTitle,fileList } = this.data;
    console.log(this.data.dataArry);
    if (this.data.selectCategory<4) {
      db.collection('communication').add({
        data: {
          dataArry: dataArry,
          selectCategory,
          creatTime,
          newsTitle,
          fileList
        },
        success: function(res) {
          wx.showToast({
            title: '发布成功',
            icon:'success'
          })
        }
      });
    }else if (this.data.selectCategory==4) {
      const { position, positionType, workLocation,creatTime } = this.data;
    db.collection('recruit').add({
      data: {
        position,
        positionType,
        workLocation,
        creatTime
      },
      success: function(res) {
        console.log('提交成功', res);
        // 添加成功后的操作
      },
      fail: function(error) {
        console.error('提交失败', error);
        // 添加失败后的操作
      }
    });
    }
  },
  blur() {
    this.editorCtx.blur()
  },
  format(e) {
    let { name, value } = e.target.dataset
    if (!name) return
    // console.log('format', name, value)
    this.editorCtx.format(name, value)

  },


  readOnlyChange() {
    this.setData({
      readOnly: !this.data.readOnly
    })
  },
  onLoad() {
    const platform = wx.getSystemInfoSync().platform
    const isIOS = platform === 'ios'
    this.setData({ isIOS})
    const that = this
    this.updatePosition(0)
    let keyboardHeight = 0
    wx.onKeyboardHeightChange(res => {
      if (res.height === keyboardHeight) return
      const duration = res.height > 0 ? res.duration * 1000 : 0
      keyboardHeight = res.height
      setTimeout(() => {
        wx.pageScrollTo({
          scrollTop: 0,
          success() {
            that.updatePosition(keyboardHeight)
            that.editorCtx.scrollIntoView()
          }
        })
      }, duration)

    })
  },
  updatePosition(keyboardHeight) {
    const toolbarHeight = 50
    const { windowHeight, platform } = wx.getSystemInfoSync()
    let editorHeight = keyboardHeight > 0 ? (windowHeight - keyboardHeight - toolbarHeight) : windowHeight
    this.setData({ editorHeight, keyboardHeight })
  },
  calNavigationBarAndStatusBar() {
    const systemInfo = wx.getSystemInfoSync()
    const { statusBarHeight, platform } = systemInfo
    const isIOS = platform === 'ios'
    const navigationBarHeight = isIOS ? 44 : 48
    return statusBarHeight + navigationBarHeight
  },
  
  insertDivider() {
    this.editorCtx.insertDivider({
      success: function () {
        console.log('insert divider success')
      }
    })
  },
  clear() {
    this.editorCtx.clear({
      success: function (res) {
        console.log("clear success")
      }
    })
  },
  removeFormat() {
    this.editorCtx.removeFormat()
  },
  insertDate() {
    const date = new Date()
    const formatDate = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
    this.editorCtx.insertText({
      text: formatDate
    })
  },
  insertImage() {
    const that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        that.editorCtx.insertImage({
          src: res.tempFilePaths[0],
          data: {
            id: 'abcd',
            role: 'god'
          },
          width: '80%',
          success: function () {
            console.log('insert image success')
          }
        })
      }
    })
  }
})